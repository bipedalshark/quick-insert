import type { SvelteComponent } from "svelte";
import type { SearchItem, SearchResult } from "./searchLib";
import { DocumentAction, DOCUMENTACTIONS } from "./actions";
import { matchFilterConfig } from "./searchFilters";
import { ContextMode, identifyContext, SearchContext } from "./contexts";
import type { SearchFilter } from "./store/Filters";

import { placeCaretAtEnd } from "./utils";
import { loadSearchIndex, QuickInsert } from "./core";

import SearchResults from "../app/SearchResults.svelte";
import SearchFiltersResults from "../app/SearchFiltersResults.svelte";
import { getActions, defaultAction } from "../module/actions";

// A search controller controls a specific search output.
// This lets us implement multiple different searches with the same search app,
// e.g. entities and filters, or maybe in the future; commands, open windows, etc.

abstract class SearchController {
  app: SearchApp;
  view?: SvelteComponent;
  results: unknown[] = [];
  selectedIndex = -1;
  selectedAction: string | null = null;

  constructor(app: SearchApp) {
    this.app = app;
  }

  public get isInsertMode(): boolean {
    return (
      this.app.attachedContext?.mode == undefined ||
      this.app.attachedContext.mode == ContextMode.Insert
    );
  }

  activate() {
    const left = this.app.attachedContext?.spawnCSS?.left;
    const tooltipSide = left !== undefined && left < 300 ? "RIGHT" : "LEFT";
    this.view?.$$set?.({ active: true, tooltips: tooltipSide });
  }
  deactivate() {
    this.view?.$$set?.({ active: false });
  }

  selectNext(): void {
    this.selectedIndex = (this.selectedIndex + 1) % this.results.length;
    this.view?.$$set?.({
      selectedIndex: this.selectedIndex,
      selectedAction: (this.selectedAction = null),
    });
  }

  selectPrevious(): void {
    this.selectedIndex =
      this.selectedIndex > 0 ? this.selectedIndex - 1 : this.results.length - 1;
    this.view?.$$set?.({
      selectedIndex: this.selectedIndex,
      selectedAction: (this.selectedAction = null),
    });
  }

  abstract onTab(index: number): void;
  abstract onEnter(index: number, evt?: JQuery.UIEventBase): void;
  abstract search(textInput: string): void;
}

class DocumentController extends SearchController {
  results: (SearchResult & {
    actions: DocumentAction[];
    defaultAction: string;
  })[] = [];
  selectedAction: string | null = null; // null means use defaultAction

  onTab(index: number): void {
    const actions = this.results[index].actions;

    if (actions.length == 0) return;
    let idx: number;
    if (this.selectedAction) {
      idx = actions.findIndex((a) => a.id == this.selectedAction);
    } else {
      idx = actions.findIndex((a) => a.id == this.results[index].defaultAction);
    }

    const nextIdx = (idx + 1) % actions.length;

    this.view?.$$set?.({
      selectedAction: (this.selectedAction = actions[nextIdx].id),
    });
  }

  onEnter(index: number, evt: JQuery.UIEventBase): void {
    // TODO: get selected action
    this.onAction(
      this.selectedAction || this.results[index].defaultAction,
      this.results[index].item,
      Boolean(evt.shiftKey)
    );
  }

  async onAction(
    actionId: string,
    item: SearchItem,
    shiftKey: boolean
  ): Promise<void> {
    console.info(
      `Quick Insert | Invoked Action [${actionId}] on [${item.name}] shiftKey:${shiftKey}`
    );
    const val = await DOCUMENTACTIONS[actionId](item);

    if (val && this.isInsertMode) {
      this.app.keepOpen = shiftKey; // Keep open until onSubmit completes
      this.app.attachedContext?.onSubmit(val);
    }

    if (this.app.attachedContext?.allowMultiple === false || !shiftKey) {
      this.app.closeDialog();
    }

    this.app.keepOpen = false;
  }

  search = (textInput: string): void => {
    if (!QuickInsert.searchLib) return;

    textInput = textInput.trim();
    if (textInput.length == 0) {
      this.view?.$$set?.({
        results: [],
        selectedIndex: (this.selectedIndex = -1),
      });
      return;
    }
    // Set a lower maximum if search is single char (single-character search is fast, but rendering is slow).
    const max = textInput.length == 1 ? 20 : 100;
    let results: SearchResult[] = [];
    if (this.app.selectedFilter) {
      if (this.app.selectedFilter.filterConfig) {
        results = QuickInsert.searchLib.search(
          textInput,
          (item) =>
            this.app.selectedFilter?.filterConfig
              ? matchFilterConfig(this.app.selectedFilter.filterConfig, item)
              : true,
          max
        );
      }
    } else {
      results = QuickInsert.searchLib.search(textInput, null, max);
    }

    if (
      this.app.attachedContext &&
      this.app.attachedContext.restrictTypes &&
      this.app.attachedContext.restrictTypes.length > 0
    ) {
      results = results.filter((i) =>
        this.app.attachedContext?.restrictTypes?.includes(i.item.documentType)
      );
    }

    this.results = results.map((res) => ({
      item: res.item,
      match: res.match,
      actions: getActions(res.item.documentType, this.isInsertMode),
      defaultAction: defaultAction(res.item.documentType, this.isInsertMode),
    }));

    this.view?.$$set?.({
      results: this.results.reverse(),
      selectedIndex: (this.selectedIndex = this.results.length - 1),
      selectedAction: (this.selectedAction = null),
    });
  };
}

class FilterController extends SearchController {
  results: SearchFilter[] = [];

  onTab(index: number): void {
    this.onEnter(index);
  }

  onEnter(index: number): void {
    this.selectFilter(this.results[index]);
  }

  selectFilter(filter: SearchFilter): void {
    this.app.setFilterTag(filter);
    this.app.selectedFilter = filter;
    this.deactivate();
    this.app.showHint(`Searching: ${filter.subTitle}`);
  }

  onClick(index: number): void {
    this.onEnter(index);
    this.app.focusInput();
  }

  search(textInput: string) {
    const cleanedInput = textInput.toLowerCase().trim();
    if (/\s$/g.test(textInput)) {
      // User has added a space after tag -> selected
      const matchingFilter = QuickInsert.filters.getFilterByTag(cleanedInput);
      if (matchingFilter) {
        this.selectFilter(matchingFilter);
        return;
      }
    }
    this.results = QuickInsert.filters.filters
      .filter((f) => !f.disabled)
      .filter((f) => f.tag.includes(cleanedInput));

    this.view?.$$set?.({
      results: this.results,
      selectedIndex: (this.selectedIndex = this.results.length - 1),
    });
  }
}

enum ActiveMode {
  Search = 1,
  Filter,
}

export class SearchApp extends Application {
  debug = false;
  mouseFocus = false;
  inputFocus = false;
  keepOpen = false;
  mode: ActiveMode = ActiveMode.Search;
  selectedFilter: SearchFilter | null = null;
  attachedContext: SearchContext | null = null;
  embeddedMode = false;

  filterController = new FilterController(this);
  documentController = new DocumentController(this);

  input?: JQuery<HTMLElement>;
  hint?: JQuery<HTMLElement>;

  text?: string;

  public get open(): boolean {
    return this._state > 0;
  }

  public get controller(): SearchController {
    if (this.mode === ActiveMode.Filter) {
      return this.filterController;
    }
    return this.documentController;
  }

  public activateMode(mode: ActiveMode): void {
    this.controller?.deactivate();
    this.mode = mode;
    this.controller?.activate();
  }

  constructor() {
    super({
      template: "modules/quick-insert/templates/quick-insert.html",
      popOut: false,
    });
  }

  resetInput(full = false): void {
    if (!full && this.selectedFilter) {
      this.setFilterTag(this.selectedFilter);
    } else {
      this.input?.html("");
    }
    this.text = undefined;
    this.focusInput();
  }

  selectNext(): void {
    this.controller?.selectNext();
  }

  selectPrevious(): void {
    this.controller?.selectPrevious();
  }

  setFilterTag(filter: SearchFilter): void {
    if (!this.input) return;
    const focus = this.input.is(":focus");

    this.input.html("");
    const editable = this.embeddedMode ? `contenteditable="false"` : "";
    $(`<span class="search-tag" ${editable}>@${filter.tag}</span>`).prependTo(
      this.input
    );
    $('<span class="breaker">&nbsp</span>').appendTo(this.input);
    if (focus) {
      this.focusInput();
    }
  }

  closeDialog(): void {
    if (this.embeddedMode) return;
    this.attachedContext?.onClose?.();
    this.selectedFilter = null;
    //@ts-expect-error tooltip not in types yet
    game.tooltip.deactivate();
    this.close();
  }

  render(
    force?: boolean,
    options?: Application.RenderOptions & { context?: SearchContext }
  ): unknown {
    if (options && options.context) {
      this.attachedContext = options.context;
      return super.render(force, options);
    }

    // Try to infer context
    const target = document.activeElement;
    if (target) {
      this.attachedContext = identifyContext(target);
    }

    if (!this.attachedContext) {
      return null;
    }

    return super.render(force, options);
  }

  showHint(notice: string): void {
    this.hint?.html(notice);
  }

  focusInput(): void {
    if (!this.input) return;
    placeCaretAtEnd(this.input.get(0));
    this.inputFocus = true;
  }

  activateListeners(html: JQuery<HTMLElement>): void {
    // (Re-)set position
    html.removeAttr("style");
    if (this.attachedContext?.spawnCSS) {
      html.css(this.attachedContext.spawnCSS);
    }
    if (this.attachedContext?.classes) {
      html.addClass(this.attachedContext.classes);
    }
    this.input = html.find(".search-editable-input");
    this.hint = html.find(".quick-insert-hint");
    this.input.on("input", () => {
      this.searchInput();
    });
    this.input.on("dragstart", (evt) => evt.stopPropagation());
    this.input.on("keydown", (evt) => {
      switch (evt.which) {
        case 13:
          return this._onKeyEnter(evt);
        case 40:
          return this._onKeyDown(evt);
        case 38:
          return this._onKeyUp(evt);
        case 27:
          return this._onKeyEsc(evt);
        case 9:
          return this._onKeyTab(evt);
        default:
          break;
      }
    });

    $(this.element).hover(
      () => {
        this.mouseFocus = true;
        this._checkFocus();
      },
      (e) => {
        if (e.originalEvent?.shiftKey) return;
        this.mouseFocus = false;
        this._checkFocus();
      }
    );

    $(this.element).on("focusout", () => {
      this.inputFocus = false;
      this._checkFocus();
    });

    $(this.element).on("focusin", () => {
      this.inputFocus = true;
      this._checkFocus();
    });

    this.focusInput();
    const node = this.element.get(0);

    if (node) {
      this.documentController.view = new SearchResults({
        target: node,
      });

      this.filterController.view = new SearchFiltersResults({
        target: node,
      });
    }

    this.documentController.view?.$on("callAction", (data) => {
      const { actionId, item, shiftKey } = data.detail;
      this.documentController.onAction(actionId, item, shiftKey);
    });

    this.filterController.view?.$on("selected", (data) => {
      const { index } = data.detail;
      this.filterController.onClick(index);
    });

    if (this.attachedContext?.filter) {
      this.activateMode(ActiveMode.Filter);
      if (typeof this.attachedContext.filter === "string") {
        const found =
          QuickInsert.filters.getFilterByTag(this.attachedContext.filter) ??
          QuickInsert.filters.getFilter(this.attachedContext.filter);
        if (found) {
          this.filterController.selectFilter(found);
        }
      } else {
        this.filterController.selectFilter(this.attachedContext.filter);
      }
    }

    if (this.attachedContext?.startText) {
      this.input.append(this.attachedContext.startText);
      this.focusInput();
      this.searchInput();
    }
    if (!QuickInsert.searchLib) {
      this.showHint(`<i class="fas fa-spinner"></i> Loading index...`);
      loadSearchIndex()
        .then(() => {
          if (this.input?.text().trim().length) {
            this.searchInput();
          } else {
            this.showHint(`Index loaded successfully`);
          }
        })
        .catch((reason) => {
          this.showHint(`Failed to load index ${reason}`);
        });
      // @ts-ignore
    } else if (QuickInsert.searchLib?.index?.fuse._docs.length == 0) {
      this.showHint(`Search index is empty for some reason`);
    }
  }

  searchInput(): void {
    if (!this.input) return;
    const text = this.input.text();
    this.text = text;

    const breaker = $(this.input).find(".breaker");

    this.showHint("");

    if (this.selectedFilter) {
      // Text was changed or breaker was removed
      if (
        !text.startsWith(`@${this.selectedFilter.tag}`) ||
        breaker.length === 0 ||
        breaker.is(":empty") ||
        breaker.html() === "<br>"
      ) {
        if (this.embeddedMode) {
          this.setFilterTag(this.selectedFilter);
          return;
        }
        // Selectedfilter doesn't match any more :(
        this.input.html(text);
        this.focusInput();
        this.selectedFilter = null;
        this.activateMode(ActiveMode.Filter);
        this.filterController.search(text.substr(1).trim());
      } else {
        this.activateMode(ActiveMode.Search);
        const search = text.replace(`@${this.selectedFilter.tag}`, "").trim();
        this.documentController.search(search);
      }
    } else if (text.startsWith("@")) {
      this.activateMode(ActiveMode.Filter);
      this.filterController.search(text.substr(1));
    } else {
      this.activateMode(ActiveMode.Search);
      this.documentController.search(text);
    }
  }

  _checkFocus = (): void => {
    if (this.debug || this.embeddedMode) return;

    if (!this.mouseFocus && !this.inputFocus && !this.keepOpen) {
      this.closeDialog();
    }
  };

  _onKeyTab = (evt: JQuery.KeyDownEvent): void => {
    evt.preventDefault();
    if (!this.embeddedMode)
      this.controller.onTab(this.controller.selectedIndex);
  };
  _onKeyEsc = (evt: JQuery.UIEventBase): void => {
    if (this.embeddedMode) return;
    evt.preventDefault();
    evt.stopPropagation();
    this.closeDialog();
  };
  _onKeyDown = (evt: JQuery.UIEventBase): void => {
    evt.preventDefault();
    this.selectNext();
  };
  _onKeyUp = (evt: JQuery.UIEventBase): void => {
    evt.preventDefault();
    this.selectPrevious();
  };
  _onKeyEnter = (evt: JQuery.UIEventBase): void => {
    evt.preventDefault();
    evt.stopImmediatePropagation();
    if (this.controller.selectedIndex > -1) {
      this.controller.onEnter(this.controller.selectedIndex, evt);
    }
  };
}
